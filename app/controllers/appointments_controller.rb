class AppointmentsController < ApplicationController
  before_action :set_appointment, only: [:show, :edit, :update, :destroy, :schedule]

  def index
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    redirect_to @appointment, notice: "Appointment saved"
  end

  def update
    redirect_to @appointment, notice: "Appointment updated"
  end

  def destroy
    redirect_to appointments_path, notice: "Appointment deleted"
  end

  def today
  end

  def schedule
    redirect_to @appointment, notice: "Appointment rescheduled"
  end

  def push_tomorrow
    redirect_to today_appointments_path, notice: "All appointments for today pushed"
  end

  def cancel_today
    redirect_to today_appointments_path, notice: "All appointments for today cancelled"
  end

  def delete_all
    redirect_to appointments_path, notice: "All appointments cancelled"
  end

  private
    def set_appointment
      @appointment = Appointment.first
    end

    def appointment_params
      params.require(:appointment).permit(:client, :date, :time_start, :time_end, :service)
    end

    def schedule_params
      schedule_parameters = params.require(:appointment).permit(date: [:day, :month, :year], time_start: [:hour, :minute], time_end: [:hour, :minute])
      date = Date.new(
        schedule_parameters[:date][:year].to_i,
        schedule_parameters[:date][:month].to_i,
        schedule_parameters[:date][:day].to_i
      )
      time_start = Time.local(
        2000, 1, 1,
        schedule_parameters[:time_start][:hour].to_i+1,
        schedule_parameters[:time_start][:minute]
      )
      time_end = Time.local(
        2000, 1, 1,
        schedule_parameters[:time_end][:hour].to_i+1,
        schedule_parameters[:time_end][:minute].to_i
      )
      {date: date, time_start: time_start, time_end: time_end}
    end
end
