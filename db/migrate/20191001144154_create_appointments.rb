class CreateAppointments < ActiveRecord::Migration[6.0]
  def change
    create_table :appointments do |t|
      t.string :client
      t.date :date
      t.time :time_start
      t.time :time_end
      t.string :service

      t.timestamps
    end
  end
end
