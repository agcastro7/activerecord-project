FactoryBot.define do
  factory :appointment do
    client { "MyString" }
    date { "2019-10-01" }
    time_start { "2019-10-01 16:41:55" }
    time_end { "2019-10-01 16:41:55" }
    service { "MyString" }
  end
end
