Rails.application.routes.draw do
  resources :appointments do
    get :today, on: :collection, as: :today
    delete :delete_all, on: :collection, as: :delete_all
    put :schedule, on: :member, as: :schedule
  end

  put :push_tomorrow, to: "appointments#push_tomorrow", as: :push_tomorrow
  delete :cancel_today, to: "appointments#cancel_today", as: :cancel_today
  root 'appointments#index'
end
